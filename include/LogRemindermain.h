/*
 * File Name:    LogRemindermain.h
 * Author:       sunowsir
 * Mail:         sunowsir@protonmail.com
 * GitHub:       github.com/sunowsir
 * Created Time: 2018年11月30日 星期五 17时03分31秒
 */

#ifndef _LOGREMINDERMAIN_H
#define _LOGREMINDERMAIN_H

#include "./needHead.h"
#include "./printLog.h"
#include "./monitor.h"

#endif
