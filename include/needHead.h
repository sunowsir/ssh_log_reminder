/*
 * File Name:    needHead.h
 * Author:       sunowsir
 * Mail:         sunowsir@protonmail.com
 * GitHub:       github.com/sunowsir
 * Created Time: 2018年11月30日 星期五 17时38分14秒
 */

#ifndef _NEEDHEAD_H
#define _NEEDHEAD_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define NORMAL 1
#define WARNING 2
#define ERROR 3

#endif
