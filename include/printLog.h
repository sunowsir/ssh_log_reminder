/*
 * File Name:    printLog.h
 * Author:       sunowsir
 * Mail:         sunowsir@protonmail.com
 * GitHub:       github.com/sunowsir
 * Created Time: 2018年11月30日 星期五 17时34分41秒
 */

#ifndef _PRINTLOG_H
#define _PRINTLOG_H

#include "./needHead.h"

/* return : success(0), false(-1). */

int printLog(int /* NORMAL, WARNING, ERROR */  , char * /* information */  );

#endif

