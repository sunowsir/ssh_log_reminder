# ssh 登录弹窗提醒

> 本脚本的`install.sh`和`uninstall.sh`参考lswc自动更换壁纸脚本的安装脚本。
> 
> 一个简单的bash脚本，当有人使用ssh登录和退出登录的时候，右上角会有弹窗通知。

1. 安装后会开机自启动。
2. 内存占用在300k左右。
3. CPU占用率会不时上浮或下沉2个百分比，尤其是当有人登录或登出的时候。


* 使用方法：
  * 安装: `bash ./install.sh`
  * 卸载: `bash ./uninstall.sh`
  * node: 不要使用`sudo`提权，若不小心使用了`sudo`提权安装那就再使用`sudo`提权卸载。

---

* [GPLv3](https://github.com/justforlxz/deepin-dreamscene/blob/master/LICENSE)

