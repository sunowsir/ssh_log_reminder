#!/bin/bash

#	File Name:    uninstall.sh
#	Author:       sunowsir
#	Mail:         sunowsir@protonmail.com
#	Created Time: 2018年10月22日 星期一 21时44分23秒


ps -aux | grep -E '\S*bash\S*\s\S*\/log_monitor.sh' | tr -s ' ' | cut -d ' ' -f 2 | xargs -I {} kill -9 {} 2> /dev/null
ps -aux | grep -E '\S*bash\S*\s\S*\/log_monitor.sh' | tr -s ' ' | cut -d ' ' -f 2 | xargs -I {} kill -9 {} 2> /dev/null
rm -rf ~/.local/scripts/log_monitor/
rm -rf ~/.config/autostart/log_monitor.desktop
echo -e "\033[1;32mBye. \033[0m"
