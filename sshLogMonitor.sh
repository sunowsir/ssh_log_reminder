#!/bin/bash


pipLogin="/etc/ssh/sshLogin";
logFile="/var/log/sshLogMonitor/sshLogMonitor.log";

# Let the program sleep for a while START
function sleepSometime() {
    local sltime=$(cat /proc/meminfo | grep -E '^Mem([T]|[A])' | grep -Eo '[0-9]*' | tr -s '\n' ' ' | awk '{
        mem = $2 / $1 * 100;
        if (mem > 75) {
            printf("1");
        } else if (mem > 55) {
            printf("3");
        } else {
            printf("5");
        }
    }');
    sleep ${sltime};
    return 0;
}
# Let the program sleep for a while END

function sendInfoToSys() {
    notify-send -i gtk-dialog-info ${1};
    return ${?};
}

function judgeHaveLogout() {
    logoutIPs="$(cat ${logFile} | grep -v "$(echo "$(w)" | grep -o "$(cat ${logFile})")")";
    return 0;
}

function main() {
    while true;
    do
        local logInInfo="$(cat ${pipLogin} | awk '
        {
            printf("%s %s\n", $2, $3);
        }
        ')";
    
        echo "${logInInfo}" >> ${logFile};
        local nowTime="$(date +"%Y-%m-%d %H:%M:%S")";
        local logIP="$(echo "${logInInfo}" | awk '{printf("%s", $2);}')";
        local logTTY="$(echo "${logInInfo}" | awk '{printf("%s", $1);}')";
        local sendInfo="\"${logIP}使用ssh登入终端${logTTY}\" \"时间：${nowTime}\"";
        sendInfoToSys ${sendInfo};
    done &;
    
    while true;
    do
        sleepSometime;
        judgeHaveLogout;
        for logoutIP in "${logoutIPs}";
        do
            logoutInfo="$(cat ${logFile} | grep "${logoutIP}")";
            local nowTime="$(date +"%Y-%m-%d %H:%M")";
            local logIP="$(echo "${logoutInfo}" | awk '{printf("%s", $2);}')";
            local logTTY="$(echo "${logoutInfo}" | awk '{printf("%s", $1);}')";
            local sendInfo="\"${logIP}使用ssh登入终端${logTTY}\" \"时间：${nowTime}\"";
            echo "$(cat ${logFile} | grep -v "${logoutIP}")" > ${logFile};
            sendInfoToSys ${sendInfo};
        done;
    done &;
    return 0;
}

