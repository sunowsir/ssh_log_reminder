#!/bin/bash

#===================================================Description and processing of Global variables. START

# 获取脚本所在位置
path="$(dirname $0)";

# 跳转到脚本所在位置
cd ${path};

# 信息存储文件 : logFile

# 当前登陆者信息 : nowInfo

# 登录者信息 : nowLogin

# 登出者信息 : nowLogout

#===================================================Description and processing of Global variables. END

#===================================================personal function START

# Determine whether the information storage file exists, if it does not exist, create the file START
function touchInfofile() {
    logFile="${path}/log_monitor.info";
    if [[ -e ${logFile} ]];then
        return 0;
    fi

    # 创建信息存储文件
    touch ${logFile};

    # 获取登陆者信息
    echo "$(who | tr -s ' ')" > ${logFile};
    return 1;
}
# Determine whether the information storage file exists, if it does not exist, create the file END

# send information function START
function send_info() {
    # ${1} : nowLogin / nowLogout
    # ${2} : 1 : log in , 2 : log out.

    local LogTTY=$(echo "${1}" | cut -d ' ' -f 2);
    local LogTime=$(echo "${1}" | cut -d ' ' -f 4);
    local LogerIP=$(echo "${1}" | cut -d ' ' -f 5);
    local str1;
    if [[ ${2} -eq 0 ]];then
        if [[ "${LogerIP}" == "(:0)" || "${LogerIP}" == "(127.0.0.1)" ]];then
            str1="本机登出${LogTTY}"
        else
            str1="来自${LogerIP}的登陆者从${LogTTY}登出";
        fi
        LogTime="$(date +"%H:%M")";
    else
        if [[ "${LogerIP}" == "(:0)" || "${LogerIP}" == "(127.0.0.1)" ]];then
            str1="本机登入${LogTTY}"
        else
            str1="来自${LogerIP}的登录者登入${LogTTY}";
        fi
    fi

    notify-send -i gtk-dialog-info "${str1}" "时间 : ${LogTime}";
    return $?;
}
# send information function END

# Let the program sleep for a while START
function sleepSometime() {
    local sltime=$(cat /proc/meminfo | grep -E '^Mem([T]|[A])' | grep -Eo '[0-9]*' | tr -s '\n' ' ' | awk '{
        mem = $2 / $1 * 100;
        if (mem > 75) {
            printf("1");
        } else if (mem > 55) {
            printf("3");
        } else {
            printf("5");
        }
    }');
    sleep ${sltime};
    return 0;
}
# Let the program sleep for a while END

# Determine if the login information has changed START
function ifContinue() {
    local now_info="$(who | tr -s ' ')";
    if [[ "${now_info}" == "$(cat ${logFile})" ]];then
        sleepSometime;
    else 
        nowInfo=${now_info};
        return 0;
    fi
}
# Determine if the login information has changed END

# Determine if someone is logged in START
function haveLogin() {
    local now_info=${1};
    local info_file="$(cat ${logFile})";
    local login_info="$(echo "${now_info}" | grep -v "${info_file}")";
    if [[ "x${login_info}" != "x" ]];then
        echo "${login_info}" >> ${logFile};
        nowLogin=${login_info};
        return 1;
    else
        # no one.
        return 0;
    fi
}
# Determine if someone is logged in END

# Determine if someone is logged out START
function haveLogout() {
    local now_info=${1};
    local logout_info="$(grep -vn "${now_info}" ${logFile})";
    local line=$(echo "${logout_info}" | cut -d ':' -f 1);
    local logout_info=$(echo "${logout_info}" | sed 's/^[0-9]*://g');
    if [[ "x${logout_info}" != "x" ]];then
        echo "$(cat ${logFile} | sed "${line}d")" > ${logFile};
        nowLogout=${logout_info};
        return 1;
    else
        # no one.
        return 0;
    fi
}
# Determine if someone is logged out END

#===================================================personal function END

function main() {
    while true;
    do
        # Determine if the information storage file exists.
        # Create this file if it does not exist.
        # prevent the file from being deleted.

        touchInfofile;
    
        # Determine the sleep time based on the memory usage.
        
        sleepSometime;
    
        # Determine whether the login information has changed.
        # If there is no change, it will not return directly.
    
        ifContinue;
    
        haveLogin "${nowInfo}";
        if [[ ${?} -eq 1 ]];then
            send_info "${nowLogin}" 1;
        fi
    
        haveLogout "${nowInfo}";
        if [[ ${?} -eq 1 ]];then
            send_info "${nowLogout}" 0;
        fi
    
    done
    return 0;
}

main;
exit 0;

