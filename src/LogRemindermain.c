/*
 * File Name:    LogRemindermain.c
 * Author:       sunowsir
 * Mail:         sunowsir@protonmail.com
 * GitHub:       github.com/sunowsir
 * Created Time: 2018年11月30日 星期五 17时03分13秒
 */

#include "../include/LogRemindermain.h"

int main(int argc, char **argv) {
    if (monitor(argc, argv) == -1) {
        if (printLog(ERROR, "monitor() error : return -1") == -1) {
            printf("\033[1;31merror\033[0mmain():printLog() return -1");
            exit(1);
        }
    }
    
    
    return 0;
}
