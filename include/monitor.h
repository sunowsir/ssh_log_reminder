/*
 * File Name:    monitor.h
 * Author:       sunowsir
 * Mail:         sunowsir@protonmail.com
 * GitHub:       github.com/sunowsir
 * Created Time: 2018年11月30日 星期五 17时40分45秒
 */

#ifndef _MONITOR_H
#define _MONITOR_H

#include "./needHead.h"

int monitor(int /* argc */ , char ** /* argv */ );

#endif
